FROM openjdk:8-jdk-alpine
EXPOSE 8787
ADD target/solution-0.0.1-SNAPSHOT.war solution-0.0.1-SNAPSHOT.war
ENV TZ Africa/Gaborone
ENTRYPOINT ["java","-jar","solution-0.0.1-SNAPSHOT.war"]
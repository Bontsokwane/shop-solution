package com.solution.controller;

import com.solution.model.Product;
import com.solution.service.Impl.ProductServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("api/shop/product")
@Api(value = "SHOP APIs")
public class ProductController {

    private  final ProductServiceImpl productService;

    public ProductController(ProductServiceImpl productService) {
        this.productService = productService;
    }

    @ApiOperation(value = "Create new Product ")
    @PostMapping(path = "/create")
    public Product createProduct(@RequestBody Product product) {

        return productService.createProduct(product);
    }

    @ApiOperation(value = "Get product by Id")
    @GetMapping("/{id}")

    public Optional<Product> getProductById(@PathVariable String id) {

        return productService.getProductById(id);
    }
}

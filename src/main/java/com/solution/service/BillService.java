package com.solution.service;

import com.solution.model.Cart;

public interface BillService {

    double netPayableAmount(Cart cart);
}

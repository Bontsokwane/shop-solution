package com.solution.service;

import com.solution.model.Product;

import java.util.Optional;

public interface ProductService {

    public Product createProduct(Product product);

    public Optional<Product> getProductById(String product_code);
}

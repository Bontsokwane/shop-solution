package com.solution.service;

import com.solution.model.Customer;

import java.util.Optional;

public interface CustomerService {

    public Customer createCustomer(Customer customer);

    public Optional<Customer> getCustomerById(String customerId);
}

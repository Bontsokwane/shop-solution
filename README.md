# shop-solution

Clone this application.

## Pre-requisite
This application runs on the below environmen:

* [Spring boot](https://spring.io/)
* [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* [Maven 3.6](https://maven.apache.org/)

## Run all test cases

JUnit and Mockito was used to make test cases.

Run [mvn clean install] to install and run all the test cases.

I used Jacoco plugin for Coverage Analysis and Reporting.After running [mvn clean install] reports directory(on this project) will be created under target/my-reports. Click index file and open it with a browser and inspect generated reports. For code quality, Sonarqube shall be used.

make changes as shown on the video then push(MASTER).

Documentation
Api documentation was done using swagger as shown below. run the application then go to (http://46.101.169.18:8888/) to view the api documentation.

Jenkins:– an open source automation server which enables developers around the world to reliably build, test, and deploy their software
http://http://46.101.169.18:8080/
username: admin
password:admin



